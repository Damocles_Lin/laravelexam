<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInfosTable extends Migration
{
    public function up()
    {
        Schema::create('userData', function (Blueprint $table) {
            $table->increments('userId');
            $table->string('userName');
            $table->string('account');
            $table->string('pw');
            $table->string('email');
            $table->softDeletes()->comment('刪除時間');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->comment('更新時間');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('建立時間');
        });
    }

    public function down()
    {
        Schema::dropIfExists('userData');
    }
}
