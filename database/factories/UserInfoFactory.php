<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\userData;
use Faker\Generator as Faker;

$factory->define(userData::class, function (Faker $faker) {
    return [
        'userName' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'pw' => $faker->password,
        'account' => $faker->userName,
    ];
});
