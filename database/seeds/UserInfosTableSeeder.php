<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Model\userData;

class UserInfosTableSeeder extends Seeder
{
    protected $user_data;

    public function __construct()
    {
        $this->user_data = new userData;
    }
    public function run()
    {
        $this->user_data->insert([
            'userName' => Str::random(10),
            'account' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'pw' => Hash::make('password'),
        ]);
    }
}
