<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace'=>'api'], function()
{
    // test1
    Route::group(['prefix'=>'exam/Ip'], function()
    {
        Route::get('/api', ['uses'=>'apiController@test1']);
    });

    // test3
    Route::group(['prefix'=>'exam/pageapi'], function()
    {
        Route::get('/', ['uses'=>'apiController@test3']);
    });
});


