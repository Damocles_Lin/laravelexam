<!DOCTYPE html>
<html lang="zh-Hant-TW">
    <head>
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="ie=edge"> -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    </head>
    <body></body>
</html>

<script>
    $(function() {
        var url = "http://127.0.0.1/api/exam/Ip/api";
        var data = null;

        $.ajax({
            url: url,
            type: 'GET',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            async: true,
            cache: false,
            dataType: 'text',
            success: function (data) {  console.log(data);
                $('body').append(data);
            }, error: function(xhr, type) {
                console.log(xhr, type);
            }
        });
    });
</script>