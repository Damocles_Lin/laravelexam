<!DOCTYPE html>
<html lang="zh-Hant-TW">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>HTML5 API Server-Send Event(伺服器發送事件)</title>
        <!-- server_event_client -->

        <script>
            window.onload = function() {
                document.querySelector('button').addEventListener('click', click, false);

                if (typeof(EventSource) !== 'undefined') {
                    // 指定 Server 檔案路徑
                    var sse = new EventSource("{{route('server')}}");

                    sse.addEventListener('open', open, false);
                    sse.addEventListener('message', message, false);
                    sse.addEventListener('error', error, false);
                } else {
                    alert('瀏覽器不支援！');
                }

                function click(event) {
                    closeEventSource();
                }

                function open(event) {
                    console.log('與 Server 正常連接！');
                }

                function message(event) {
                    // var pullData = JSON.parse(event.data); console.log(pullData);
                    var newElement = document.createElement('li');
                    newElement.innerHTML = event;
                    document.body.appendChild(newElement);
                }

                function error(event) {
                    closeEventSource();
                    alert('連接發生錯誤！');
                };

                function closeEventSource() {
                    sse.close();
                    alert('已中斷 Server 連線！');
                }
            }
        </script>
    </head>
    <body>
        <button>中斷 Server 連線</button>
    </body>
</html>