<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class userData extends Model
{
    use SoftDeletes;

    protected $table = 'userdata';
    protected $primaryKey = 'userId ';
    public $timestamps = true;
    protected $casts = [
        'userId' => 'string',
        'userName' => 'string',
        'account' => 'string',
        'pw' => 'string',
        'email' => 'string',
    ];
    protected $guarded = [
        'deleted_at',
        'updated_at',
        'created_at'
    ];
    protected $hidden = [];
}
