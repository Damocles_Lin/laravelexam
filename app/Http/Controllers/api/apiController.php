<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\userData;

class apiController extends Controller
{
    protected $user_data;

    public function __construct()
    {
        $this->user_data = new userData;
    }

    public function test1()
    {
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

        ob_end_clean(); // 清空快取
        ob_start(); // 開始緩衝資料

        $interval = env('LONGPOLLING_INTERVAL', 10);
        $chk = env('LONGPOLLING_CHK', 1);
        $sum = $chk;
        $lastest_id = $this->user_data->first()->userId;

        do {
            echo '時間:'.date("Y/m/d H:i:s").'<br>';

            // 判斷是否有新的資料
            $query_id = $this->user_data->first()->userId;
            if ( $lastest_id !== $query_id ) {
                $lastest_id = $query_id;
                echo '時間:'.date("Y/m/d H:i:s").' 有更新<br>'; // 時間:xx/xx/xx xx:xx:xx 有更新
            }

            ob_flush();
            flush();

            $sum += $chk;
            sleep($chk);
            if ( $sum >= $interval ) {
                die('時間:'.date("Y/m/d H:i:s").'<br>');
            }
        } while(/* $sum <= $interval */1); // false return
    }

    public function test3()
    {
        $this->user_data->insert([
            'userName' => 'test',
            'account' => 'test',
            'pw' => 'test',
            'email' => 'test@test.com'
        ]);
    }
}
